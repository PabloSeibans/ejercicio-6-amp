import { Component, OnInit } from '@angular/core';
import { Juego } from 'src/app/services/juego';

@Component({
  selector: 'app-juego',
  templateUrl: './juego.component.html',
  styleUrls: ['./juego.component.css'],
  providers: [Juego]
})
export class JuegoComponent implements OnInit {
  constructor(public juego: Juego) {}

  ngOnInit(): void {
  }

  InicioJuego():void {
    this.juego.StartGame();
    const JugadorActual = 'Turno del Jugador: '+ this.juego.Turno;
    const informacion = document.querySelector('.status-actual');
  }

  async clickSubir( subirarchivo: any ): Promise<void> {
    if (this.juego.EstadoJuego === 1) {
      const position = subirarchivo.currentTarget.getAttribute('position');

      this.juego.setField(position, this.juego.Turno);

      const color = this.juego.getPlayerColorClass();
      subirarchivo.currentTarget.classList.add(color);
    }

    this.juego.CambiarJugador();
  }

}
