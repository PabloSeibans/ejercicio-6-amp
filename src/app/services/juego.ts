import { Status } from './statusdejuego';

export class Juego {

    juegoField: Array<number> = [];

    Turno!: number;

    EstadoJuego: Status;

    public constructor(){
        this.EstadoJuego = Status.STOP;
        this.juegoField = [0,0,0,0,0,0,0,0,0];
    }

    StartGame():void{
        this.juegoField = [0,0,0,0,0,0,0,0,0];
        this.Turno = this.JugadorRandom(); 
        console.log(this.Turno);
        
        this.EstadoJuego = Status.START;
    }

    JugadorRandom():number{
        const ComienzoJugador = Math.floor(Math.random() * 2) + 1;
        return ComienzoJugador;
    }

    setField(posicion: number, value: number):void{
        this.juegoField[posicion] = value;
    }

    getPlayerColorClass():string{
        const colorClass = (this.Turno === 2)?'player-1':'player-2';
        return colorClass;
    }

    CambiarJugador():void{
        this.Turno = (this.Turno === 2)? 1:2;
    }
}
